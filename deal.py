from abc import ABC


class Sell(ABC):
    def __init__(self, price_per_meter, discount=False, convertible=False,
                 *args, **kwargs):
        self.price_per_meter = price_per_meter
        self.discount = discount
        self.convertible = convertible
        super().__init__(*args, **kwargs)


class Rent(ABC):
    def __init__(self, initial_price, monthly_price, convertible, *args, **kwargs):
        self.initial_price = initial_price
        self.monthly_price = monthly_price
        self.convertible = convertible
        super().__init__(*args, **kwargs)
