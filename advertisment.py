from base import BaseClass
from estate import Apartment, House, Store
from deal import Sell, Rent


class ApartmentSell(BaseClass, Apartment, Sell):

    def __str__(self):
        return self.id

    def show_detail(self):

        return f'id :{self.id}\taddress :{self.address}\n' \
               f'area:{self.area}\trooms :{self.rooms_count}\t' \
               f'price :{self.price_per_meter * self.area}$\t' \
               f'price per meter :{self.price_per_meter}$\n' \
               f'discount :{self.discount}\tconvertible :{self.convertible}\n'\
               f'floor :{self.floor}\t' \
               f'parking:{self.parking}\televator:{self.elevator}\n' \
               f'build year :{self.build_year}'


class ApartmentRent(BaseClass, Apartment, Rent):

    def __str__(self):
        return self.id

    def show_detail(self):

        return f'id :{self.id}\taddress :{self.address}\n' \
               f'total price :{self.initial_price + self.monthly_price * 12}$\t' \
               f'initial price :{self.initial_price}$\t' \
               f'monthly price :{self.monthly_price}$\t' \
               f'convertible :{self.convertible}\n' \
               f'area :{self.area}\trooms :{self.rooms_count}\n' \
               f'build year :{self.build_year}'


class HouseSell(BaseClass, House, Sell):

    def __str__(self):
        return self.id

    def show_detail(self):

        return f'id :{self.id}\taddress :{self.address}\n' \
               f'area :{self.area}\tfloor_count :{self.floor_count}\t' \
               f'price :{self.price_per_meter * self.area}$\t' \
               f'price per meter :{self.price_per_meter}$\t' \
               f'discount :{self.discount}\n' \
               f'build year :{self.build_year}'


class HouseRent(BaseClass, House, Rent):

    def __str__(self):
        return self.id

    def show_detail(self):

        return f'id :{self.id}\taddress :{self.address}\n' \
               f'area :{self.area}\tfloor_count :{self.floor_count}\t' \
               f'total price :{self.initial_price + self.monthly_price * 12}$\t' \
               f'initial price :{self.initial_price}$\t' \
               f'monthly price :{self.monthly_price}$\n' \
               f'convertible :{self.convertible}\n' \
               f'build year :{self.build_year}'


class StoreSell(BaseClass, Store, Sell):

    def __str__(self):
        return self.id

    def show_detail(self):

        return f'id :{self.id}\taddress :{self.address}\n' \
               f'area :{self.area}\t' \
               f'price : {self.price_per_meter * self.area}$\t' \
               f'price per meter : {self.price_per_meter}$\n' \
               f'discount : {self.discount}'


class StoreRent(BaseClass, Store, Rent):

    def __str__(self):
        return self.id

    def show_detail(self):

        return f'id :{self.id}\taddress :{self.address}\n' \
               f'total price :{self.initial_price + self.monthly_price * 12}$\t' \
               f'initial price :{self.initial_price}$\t' \
               f'monthly price : {self.monthly_price}$\t' \
               f'convertible : {self.convertible}'
