class Manager:
    def __init__(self, _class):
        self._class = _class

    def __str__(self):
        return f'manager: {self._class}'

    def search(self, **kwargs):

        results = list()
        set_min = set()
        set_max = set()
        set_mix = set()

        # search for two key
        # search for one key
        for key, value in kwargs.items():
            if len(kwargs) == 2:

                if key.endswith('__min'):
                    key = key[:-5]
                    compare_key = 'min'

                if key.endswith('__max'):
                    key = key[:-5]
                    compare_key = 'max'

                for obj in self._class.object_list:

                    if hasattr(obj, key):

                        if compare_key == 'min':
                            if getattr(obj, key) >= value:
                                set_min.add(obj)

                        if compare_key == 'max':
                            if getattr(obj, key) <= value:
                                set_max.add(obj)

                        for i in set_min:
                            if i in set_max:
                                set_mix.add(i)
                                return set_mix

            if len(kwargs) == 1:
                if key.endswith('__min'):
                    key = key[:-5]
                    compare_key = 'min'
                elif key.endswith('__max'):
                    key = key[:-5]
                    compare_key = 'max'
                else:
                    compare_key = 'equal'

                for obj in self._class.object_list:

                    if hasattr(obj, key):

                        if compare_key == 'min':
                            result = bool(getattr(obj, key) >= value)
                        elif compare_key == 'max':
                            result = bool(getattr(obj, key) <= value)
                        else:
                            result = bool(getattr(obj, key) == value)

                        if result:
                            results.append(obj)
        return results
