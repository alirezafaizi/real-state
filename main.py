from user import User
from region import Region
from advertisment import ApartmentSell

reg1 = Region(name='zone 1')
user1 = User(first_name='alireza', last_name='faizi', phone_number='099032265')

if __name__ == '__main__':

    apartment_sell = ApartmentSell(
        elevator=True, parking=True, floor=3, area=300, rooms_count=4,
        build_year=2020, address='call of duty', price_per_meter=1000,
        discount=True, convertible=False,
    )

    print(apartment_sell.show_detail())
    #print(ApartmentSell.manager.search(rooms_count=4))
    print(ApartmentSell.manager.search(price_per_meter__min=900,
                                       price_per_meter__max=1500))
