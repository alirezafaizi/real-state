from abc import ABC


class Body(ABC):
    def __init__(self, area, rooms_count, build_year, address, *args, **kwargs):
        self.area = area
        self.rooms_count = rooms_count
        self.build_year = build_year
        self.address = address
        super().__init__(*args, **kwargs)


class Apartment(Body):
    def __init__(self, floor, elevator=False, parking=False, *args, **kwargs):
        self.elevator = elevator
        self.parking = parking
        self.floor = floor
        super().__init__(*args, **kwargs)


class House(Body):
    def __init__(self, yard, floor_count, *args, **kwargs):
        self.yard = yard
        self.floor_count = floor_count
        super().__init__(*args, **kwargs)


class Store(Body):
    pass
